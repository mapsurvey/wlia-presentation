library(leaflet)
library(leaflet.extras)
library(sf)
library(dplyr)

## define EXAMPLE data
#data <- "C:/Users/Carter/Documents/UWEC-2020/Research/example-data"
data <- "/home/matt/git-repos/mapsurvey/example-data/prm-test"

## specify type of file and data path to create list of file directories
file.list <- list.files(pattern = "*.geojson", path = data, full.names = TRUE)

## loop for reading and binding shp files
for (i in 1:length(file.list)) {
  if (i == 1) {
    spdf <- sf::st_read(file.list[i], quiet = TRUE)
  } else {
    tmp.spdf <- sf::st_read(file.list[i], quiet = TRUE)
    spdf <- rbind(spdf, tmp.spdf)
  }
}

## looks like they are all polygons; TODO remove this line
print(spdf, n = nrow(spdf))

## but when checking for valid geometries, a bunch are no good; probably some
## intersecting polygons here; TODO remove this line
sf::st_is_valid(spdf)

## try to make them valid, it should work
spdf.valid <- lwgeom::st_make_valid(spdf)

## but now there are a bunch of things that are not polygons in here which might
## cause problems; it works fine with my example below but might not work with
## the other stuff you're doing; TODO remove this line
print(spdf.valid, n = nrow(spdf.valid))

## need to drop duplicates
spdf.valid.no.dup <- dplyr::distinct(spdf.valid)

## get just polygons
spdf.poly <- spdf.valid.no.dup %>%
  filter(
    st_geometry_type(.) %in%
    c("POLYGON", "MULTIPOLYGON"))

## this can all by simplified a bit (you can remove lines 23 - 46)
spdf.poly <- spdf %>%
  lwgeom::st_make_valid() %>%
  dplyr::distinct() %>%
  filter(
    st_geometry_type(.) %in%
    c("POLYGON", "MULTIPOLYGON"))


## create leaflet map; i actually think it's a good idea to use stamen toner
## here so that the color of the polygons does not conflict with that of the
## basemap. notice how a few people included Oklahoma, Arkansas, and Kentucky
## but that's not really visible with stamen terrain. for the
## smallest/largest/nsew though, stamen terrain is fine
leaflet(spdf.poly) %>%
  addProviderTiles(providers$Stamen.TonerBackground) %>%
  addPolygons(stroke = FALSE,
              fill = TRUE,
              fillOpacity = 0.03)

## it would be awesome to convert this to a raster, make cell values equal to
## the number of intersecting polygons and use a polychromatic color scheme
## instead of one single color, but we don't have time...
